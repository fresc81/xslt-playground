#!/bin/bash

inFile=graph.xml
outType=svg
xsltproc graph.xslt /dev/stdin < ${inFile} | tee /dev/stderr | dot -T${outType} > graph.${outType} 
