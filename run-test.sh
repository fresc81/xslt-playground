#!/bin/bash

indir=./data
outdir=./report

mkdir -p $outdir

toc="<ul>"
for h in $(basename -s .xml -a $indir/*.xml); do
  toc+="<li><a href='$h.html'>$h</a></li>"
done
toc+="</ul>"

for f in $indir/*.xml; do
  xslterr=$(mktemp)
  outname=$(basename "$f" .xml)
  outfile=$outdir/$outname
  echo -n converting $outname...
  xsltproc --stringparam toc "$toc" --stringparam self "$outname" -v -o "$outfile.html" test.xslt "$f" 2> "$xslterr" || cat "$xslterr"
  rm "$xslterr"
  echo " done."
done
