<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:exsl="http://exslt.org/common"
    xmlns:func="http://exslt.org/functions"
    xmlns:my="http://my.uri"
    xmlns="http://my.uri"
    extension-element-prefixes="exsl func"
    exclude-result-prefixes="my"
>
  <xsl:output
      method="text"
      indent="yes"
      encoding="UTF-8"
  />

  <xsl:key
    name="id"
    match="graph|digraph|node|edge|subgraph"
    use="@id"
  />

  <func:function name="my:edgeop">
    <xsl:param name="dir"/>
    <xsl:choose>
      <xsl:when test="$dir='forward'"><func:result><xsl:text disable-output-escaping="yes">-&gt;</xsl:text></func:result></xsl:when>
      <xsl:when test="$dir='backward'"><func:result><xsl:text disable-output-escaping="yes">&lt;-</xsl:text></func:result></xsl:when>
      <xsl:when test="$dir='both'"><func:result><xsl:text disable-output-escaping="yes">&lt;-&gt;</xsl:text></func:result></xsl:when>
      <xsl:when test="$dir='none'"><func:result><xsl:text disable-output-escaping="yes">--</xsl:text></func:result></xsl:when>
    </xsl:choose>
  </func:function>

  <func:function name="my:default">
    <xsl:param name="value"/>
    <xsl:param name="default"/>
    <xsl:choose>
      <xsl:when test="$value"><func:result><xsl:value-of select="$value"/></func:result></xsl:when>
      <xsl:otherwise><func:result><xsl:value-of select="$default"/></func:result></xsl:otherwise>
    </xsl:choose>
  </func:function>

  <func:function name="my:has-from-to">
    <xsl:param name="current"/>
    <func:result><xsl:value-of select="(boolean($current/@from) or boolean($current/@from-cluster)) and (boolean($current/@to) or boolean($current/@to-cluster))"/></func:result>
  </func:function>

  <xsl:template name="dotfile">
    <xsl:param name="graph"/>
    <xsl:param name="defaultDir"/>
    <xsl:if test="@strict='yes'">strict </xsl:if> <xsl:value-of select="$graph"/> <xsl:if test="@id">"<xsl:value-of select="@id"/>"</xsl:if> {
      <xsl:apply-templates select="node|edge|attr|subgraph">
        <xsl:with-param name="defaultDir" select="$defaultDir"/>
      </xsl:apply-templates>
    }
  </xsl:template>

  <xsl:template match="node">
    <xsl:param name="defaultDir"/>
    <xsl:choose>
      <xsl:when test="@id">"<xsl:value-of select="@id"/>"</xsl:when>
      <xsl:otherwise>node</xsl:otherwise>
    </xsl:choose> [ <xsl:apply-templates select="attr">
      <xsl:with-param name="defaultDir" select="$defaultDir"/>
    </xsl:apply-templates> ];
  </xsl:template>

  <xsl:template match="edge">
    <xsl:param name="defaultDir"/>
    <xsl:choose>
      <xsl:when test="my:has-from-to(.)='true'">
        "<xsl:value-of select="key('id', @from)/@id"/>" <xsl:value-of select="my:edgeop(my:default(@direction, $defaultDir))"/> "<xsl:value-of select="key('id', @to)/@id"/>"
      </xsl:when>
      <xsl:otherwise>edge</xsl:otherwise>
    </xsl:choose> [ <xsl:apply-templates select="attr">
      <xsl:with-param name="defaultDir" select="$defaultDir"/>
    </xsl:apply-templates>];
  </xsl:template>

  <xsl:template match="attr">
    <xsl:param name="defaultDir"/>
    <xsl:value-of select="@name"/>="<xsl:apply-templates><xsl:with-param name="defaultDir" select="$defaultDir"/></xsl:apply-templates>";
  </xsl:template>

  <xsl:template match="subgraph">
    <xsl:param name="defaultDir"/>
    subgraph <xsl:if test="@id">"<xsl:if test="@cluster='yes'">cluster_</xsl:if><xsl:value-of select="@id"/>"</xsl:if> {
    <xsl:apply-templates select="node|edge|attr|subgraph"><xsl:with-param name="defaultDir" select="$defaultDir"/></xsl:apply-templates>};
  </xsl:template>

  <xsl:template match="/graph">
    <xsl:call-template name="dotfile">
      <xsl:with-param name="graph">graph </xsl:with-param>
      <xsl:with-param name="defaultDir" select="'none'"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="/digraph">
    <xsl:call-template name="dotfile">
      <xsl:with-param name="graph">digraph </xsl:with-param>
      <xsl:with-param name="defaultDir" select="'forward'"/>
    </xsl:call-template>
  </xsl:template>

</xsl:stylesheet>
