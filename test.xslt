<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:html="http://www.w3.org/1999/xhtml"
    xmlns:date="http://exslt.org/dates-and-times"
    xmlns:exsl="http://exslt.org/common"
    xmlns="http://www.w3.org/1999/xhtml"
    extension-element-prefixes="exsl date"
    exclude-result-prefixes="html"
>

<xsl:param name="toc" select="'nicht vorhanden'"/>
<xsl:param name="self" select="'unbenannt'"/>
<xsl:output
    method="xml"
    doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"
    doctype-public="-//W3C//DTD XHTML 1.1//EN"
    indent="yes"
    encoding="UTF-8"
/>

<xsl:template match="/test/table/row">
<tr>
  <td class="index"><xsl:value-of select="@index" /></td>
  <td class="title"><xsl:value-of select="field[@name='title']" /></td>
  <td class="subtitle"><xsl:value-of select="field[@name='subtitle']" /></td>
  <td class="crdate"><xsl:value-of select="date:add('1970-01-01T00:00:00Z', date:duration(field[@name='crdate']))" /></td>
  <td class="update"><xsl:value-of select="date:add('1970-01-01T00:00:00Z', date:duration(field[@name='update']))" /></td>
</tr>
</xsl:template>

<xsl:template match="/">
<html>
  <head>
    <meta charset="UTF-8"/>
    <title>Bericht - <xsl:value-of select="$self"/></title>
    <style>
      table, tr, th, td { border: 1px solid black }
      th { background: black; color: white; font-weight: bold }
    </style>
  </head>
  <body>
    <h1>Bericht - <em><xsl:value-of select="$self"/></em></h1>
    <h2>Navigation</h2>
    <p><xsl:value-of select="$toc" disable-output-escaping="yes"/></p>
    <h2>Tabelle</h2>
    <table>
      <tr>
        <th>id</th>
        <th>Titel</th>
        <th>Untertitel</th>
        <th>Erstellt</th>
        <th>Geändert</th>
      </tr>
      <xsl:apply-templates select="test/table/row"/>
    </table>
    <script>
      if (typeof(Array.prototype.forEach) !== 'function') {
        Array.prototype.forEach = function (cb) {
          for (var i=0; i <xsl:text disable-output-escaping="yes"><![CDATA[<]]></xsl:text> this.length; i++) {
            cb(this[i], i, this);
          }
        }
      }
      window.onload = function () {
        Array.prototype.forEach.call(
          document.querySelectorAll('td.crdate,td.update'),
          function (elem) {
            elem.innerHTML = new Date(elem.innerHTML).toLocaleString(undefined, {
              formatMatcher: 'basic',
              year: 'numeric',
              month: '2-digit',
              day: '2-digit',
              hour: '2-digit',
              minute: '2-digit'
            });
          }
        )
      }
    </script>
  </body>
</html>
</xsl:template>

</xsl:stylesheet>